package consulting.hmd.model;

import java.time.LocalDateTime;
import java.util.List;

import consulting.hmd.model.common.EntityBase;

public class DetalleDocumentosEnviados extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private Parametros motivoDocumento;
	private CorrespondenciaRecibida correspondenciaRecibida;
	private Empleado empleadoRecibido;
	private Empleado empleadoEnvio;
	private Parametros estadoTransaccion;
	private LocalDateTime fechaTransaccion;
	private LocalDateTime fechaEnvio;
	private boolean esCopia;
	private List<DetalleObservacionesReasignacionesDocumentos> detallesObservacionesReasingacionesDocumentos;
	private List<DetalleActividadesDocumentoEmpleados> detallesActividadesDocumentosEmpleados;

	public Parametros getMotivoDocumento() {
		return motivoDocumento;
	}

	public void setMotivoDocumento(Parametros motivoDocumento) {
		this.motivoDocumento = motivoDocumento;
	}

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public Empleado getEmpleadoRecibido() {
		return empleadoRecibido;
	}

	public void setEmpleadoRecibido(Empleado empleadoRecibido) {
		this.empleadoRecibido = empleadoRecibido;
	}

	public Empleado getEmpleadoEnvio() {
		return empleadoEnvio;
	}

	public void setEmpleadoEnvio(Empleado empleadoEnvio) {
		this.empleadoEnvio = empleadoEnvio;
	}

	public Parametros getEstadoTransaccion() {
		return estadoTransaccion;
	}

	public void setEstadoTransaccion(Parametros estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}

	public LocalDateTime getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(LocalDateTime fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public LocalDateTime getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(LocalDateTime fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public boolean isEsCopia() {
		return esCopia;
	}

	public void setEsCopia(boolean esCopia) {
		this.esCopia = esCopia;
	}

	public List<DetalleObservacionesReasignacionesDocumentos> getDetallesObservacionesReasingacionesDocumentos() {
		return detallesObservacionesReasingacionesDocumentos;
	}

	public void setDetallesObservacionesReasingacionesDocumentos(
			List<DetalleObservacionesReasignacionesDocumentos> detallesObservacionesReasingacionesDocumentos) {
		this.detallesObservacionesReasingacionesDocumentos = detallesObservacionesReasingacionesDocumentos;
	}

	public List<DetalleActividadesDocumentoEmpleados> getDetallesActividadesDocumentosEmpleados() {
		return detallesActividadesDocumentosEmpleados;
	}

	public void setDetallesActividadesDocumentosEmpleados(
			List<DetalleActividadesDocumentoEmpleados> detallesActividadesDocumentosEmpleados) {
		this.detallesActividadesDocumentosEmpleados = detallesActividadesDocumentosEmpleados;
	}

}
