package consulting.hmd.services.common;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;

@Service
public class ServiceImpl<T, ID> implements IService<T, ID> {

	@Autowired
	private JpaRepository<T, ID> _repo;

	public ServiceImpl(JpaRepository<T, ID> repo) {
		this._repo = repo;
	}
	
	@Override
	public Integer Salvar(T ob, Acciones accion) throws ServiceException {
		try {
			
			switch (accion) {
			case CREAR:
				_repo.save(ob);
				return 201;	
				
			case MODIFICAR:
				_repo.save(ob);
				return 200;	
				
			default:
				throw new ServiceException(500,"Opción Invalida");
			}
			
		} catch (Exception e) {
			throw new ServiceException(500, e.getCause().getMessage());
		}
	}
	
	@Override
	public Integer Eliminar(ID id) throws ServiceException {
		try {
			_repo.deleteById(id);
			return 200;
		} catch (Exception e) {
			throw new ServiceException(500, e.getCause().getMessage());
		}
	}

	@Override
	public T ObtenerPorId(ID id) throws Exception {
		Optional<T> ob = _repo.findById(id);
		return ob.isPresent() ? ob.get() : null;
	}

	@Override
	public List<T> Obtener() throws Exception {
		return _repo.findAll();
	}
}