package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.UnidadesEmisoras;

@Repository
public interface IInstitucionesEmisorasRepository extends JpaRepository<UnidadesEmisoras, Integer> {

}
