package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleDocumentosRecibidos;

@Repository
public interface IDetalleDocumentosRecibidosRepository extends JpaRepository<DetalleDocumentosRecibidos, Integer> {

}
