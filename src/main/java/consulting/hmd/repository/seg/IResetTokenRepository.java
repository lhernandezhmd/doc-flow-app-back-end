package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;

import consulting.hmd.model.seg.ResetToken;



public interface IResetTokenRepository extends JpaRepository<ResetToken, Long> {

	ResetToken findByToken(String token);

}
