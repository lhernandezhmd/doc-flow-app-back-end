package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.Configuracion;

@Repository
public interface IConfiguracionRepository extends JpaRepository<Configuracion, Integer> {

}
