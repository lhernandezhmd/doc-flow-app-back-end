package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.Rol;


@Repository
public interface IRolRepo extends JpaRepository<Rol, Integer> {

}
