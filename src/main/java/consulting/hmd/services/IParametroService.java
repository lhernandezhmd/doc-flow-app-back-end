package consulting.hmd.services;



import consulting.hmd.model.Parametros;
import consulting.hmd.services.common.IServices;


public interface IParametroService extends IServices<Parametros, Integer> {

}
