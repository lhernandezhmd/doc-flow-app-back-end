package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.Usuario;


@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {

}
