package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.TipoParametros;

@Repository
public interface ITipoParametroRepository extends JpaRepository<TipoParametros, Integer> {

}
