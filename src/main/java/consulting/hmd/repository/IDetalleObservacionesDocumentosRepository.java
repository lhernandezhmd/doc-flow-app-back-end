package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleObservacionesDocumento;

@Repository
public interface IDetalleObservacionesDocumentosRepository
		extends JpaRepository<DetalleObservacionesDocumento, Integer> {

}
