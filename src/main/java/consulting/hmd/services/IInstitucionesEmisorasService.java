package consulting.hmd.services;



import consulting.hmd.model.UnidadesEmisoras;
import consulting.hmd.services.common.IServices;


public interface IInstitucionesEmisorasService extends IServices<UnidadesEmisoras, Integer> {

}
