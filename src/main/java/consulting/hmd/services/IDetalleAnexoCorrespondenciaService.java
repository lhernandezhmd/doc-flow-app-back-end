package consulting.hmd.services;


import consulting.hmd.model.DetalleAnexoCorrespondencia;
import consulting.hmd.services.common.IServices;


public interface IDetalleAnexoCorrespondenciaService extends IServices<DetalleAnexoCorrespondencia, Integer> {

}
