package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleCopiasDocumento;

@Repository
public interface IDetalleCopiasDocumentoRepository extends JpaRepository<DetalleCopiasDocumento, Integer> {

}
