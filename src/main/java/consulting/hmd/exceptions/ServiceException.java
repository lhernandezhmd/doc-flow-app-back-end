package consulting.hmd.exceptions;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	private int codigo;

	public ServiceException(int codigo, String error) {
		super(error);
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}