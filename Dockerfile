FROM openjdk:8-jdk-alpine
LABEL mantainer="Lester Eduardo Hernandez"
WORKDIR /workspace
ADD target/academics-app-backend.jar academics-app-backend.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file/dev/./urandom -jar /workspace/academics-app-backend.jar 
EXPOSE 8080