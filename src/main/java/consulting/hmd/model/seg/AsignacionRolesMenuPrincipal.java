package consulting.hmd.model.seg;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.hmd.model.common.EntityBase;


@Entity
@Table(name = "seg_rol_menu_principal", schema = "public")
public class AsignacionRolesMenuPrincipal extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "id_rol", nullable = false)
	@JsonIgnore
	private Rol rol;
	@ManyToOne
	@JoinColumn(name = "id_menu_principal", nullable = false)
	@JsonIgnore
	private MenuPrincipal menuPrincipal;

	public AsignacionRolesMenuPrincipal() {
	}
	
	public AsignacionRolesMenuPrincipal(int id) {
		setId(id);
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public MenuPrincipal getMenuPrincipal() {
		return menuPrincipal;
	}

	public void setMenuPrincipal(MenuPrincipal menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
}