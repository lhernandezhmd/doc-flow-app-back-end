package consulting.hmd.model.seg;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import consulting.hmd.model.common.EntityBase;


@Entity
@Table(name = "seg_roles", schema = "public")
public class Rol extends EntityBase<Integer> implements Serializable {

	private static final long serialVersionUID = 1L;

	public Rol() {
	}

	public Rol(int id) {
		setId(id);
	}

	@Column(name = "nombre", nullable = false, length = 20)
	private String nombre;
	@Column(name = "descripcion", nullable = false, length = 1000)
	private String descripcion;

	@OneToMany(mappedBy = "rol", fetch = FetchType.LAZY)
	private List<AsignacionRolesUsuarios> roles;


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<AsignacionRolesUsuarios> getRoles() {
		return roles;
	}

	public void setRoles(List<AsignacionRolesUsuarios> roles) {
		this.roles = roles;
	}

}
