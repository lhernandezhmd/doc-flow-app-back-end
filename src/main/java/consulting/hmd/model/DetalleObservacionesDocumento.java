package consulting.hmd.model;

import java.time.LocalDateTime;

import consulting.hmd.model.common.EntityBase;

public class DetalleObservacionesDocumento extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private CorrespondenciaRecibida correspondenciaRecibida;
	private Empleado empleado;
	private LocalDateTime fechaObservacion;
	private String observacion;

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public LocalDateTime getFechaObservacion() {
		return fechaObservacion;
	}

	public void setFechaObservacion(LocalDateTime fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

}
