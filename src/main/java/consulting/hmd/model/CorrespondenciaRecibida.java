package consulting.hmd.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import consulting.hmd.model.common.EntityBase;

public class CorrespondenciaRecibida extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;
	private Parametros tipoDocumento;
	private Parametros tipoCorrespondencia;
	private UnidadesEmisoras unidadEmisora;
	private UnidadesReceptoras unidadReceptora;
	private String numeroDocumento;
	private String personaRemitente;
	private LocalDate fechaDocumento;
	private LocalDate fechaRecepcion;
	private LocalDateTime fechaIngreso;

	private String extencionArchivoImagen;
	private String urlPdf;

	private List<DetalleActividadesDocumentoEmpleados> detalleActividadesDocumentoUsuario;
	private List<DetalleObservacionesReasignacionesDocumentos> detalleObservacionesReasignacionesDocumento;
	private List<DetalleDocumentosRecibidos> detalleDocumentosRecibidos;
	private List<DetalleDocumentosEnviados> detalleDocumentosEnviados;

	private List<DetalleAnexoCorrespondencia> detalleAnexoCorrespondencias;
	private List<DetalleObservacionesDocumento> detalleObseravacionesDocumento;
	private List<DetalleCopiasDocumento> detalleCopiasDocumentos;

	public CorrespondenciaRecibida(int id) {
		setId(id);
	}

	public CorrespondenciaRecibida() {
	}

	public Parametros getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Parametros tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Parametros getTipoCorrespondencia() {
		return tipoCorrespondencia;
	}

	public void setTipoCorrespondencia(Parametros tipoCorrespondencia) {
		this.tipoCorrespondencia = tipoCorrespondencia;
	}

	public UnidadesEmisoras getUnidadEmisora() {
		return unidadEmisora;
	}

	public void setUnidadEmisora(UnidadesEmisoras unidadEmisora) {
		this.unidadEmisora = unidadEmisora;
	}

	public UnidadesReceptoras getUnidadReceptora() {
		return unidadReceptora;
	}

	public void setUnidadReceptora(UnidadesReceptoras unidadReceptora) {
		this.unidadReceptora = unidadReceptora;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getPersonaRemitente() {
		return personaRemitente;
	}

	public void setPersonaRemitente(String personaRemitente) {
		this.personaRemitente = personaRemitente;
	}

	public LocalDate getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(LocalDate fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public LocalDate getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(LocalDate fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public LocalDateTime getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDateTime fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getExtencionArchivoImagen() {
		return extencionArchivoImagen;
	}

	public void setExtencionArchivoImagen(String extencionArchivoImagen) {
		this.extencionArchivoImagen = extencionArchivoImagen;
	}

	public String getUrlPdf() {
		return urlPdf;
	}

	public void setUrlPdf(String urlPdf) {
		this.urlPdf = urlPdf;
	}

	public List<DetalleActividadesDocumentoEmpleados> getDetalleActividadesDocumentoUsuario() {
		return detalleActividadesDocumentoUsuario;
	}

	public void setDetalleActividadesDocumentoUsuario(
			List<DetalleActividadesDocumentoEmpleados> detalleActividadesDocumentoUsuario) {
		this.detalleActividadesDocumentoUsuario = detalleActividadesDocumentoUsuario;
	}

	public List<DetalleObservacionesReasignacionesDocumentos> getDetalleObservacionesReasignacionesDocumento() {
		return detalleObservacionesReasignacionesDocumento;
	}

	public void setDetalleObservacionesReasignacionesDocumento(
			List<DetalleObservacionesReasignacionesDocumentos> detalleObservacionesReasignacionesDocumento) {
		this.detalleObservacionesReasignacionesDocumento = detalleObservacionesReasignacionesDocumento;
	}

	public List<DetalleDocumentosRecibidos> getDetalleDocumentosRecibidos() {
		return detalleDocumentosRecibidos;
	}

	public void setDetalleDocumentosRecibidos(List<DetalleDocumentosRecibidos> detalleDocumentosRecibidos) {
		this.detalleDocumentosRecibidos = detalleDocumentosRecibidos;
	}

	public List<DetalleDocumentosEnviados> getDetalleDocumentosEnviados() {
		return detalleDocumentosEnviados;
	}

	public void setDetalleDocumentosEnviados(List<DetalleDocumentosEnviados> detalleDocumentosEnviados) {
		this.detalleDocumentosEnviados = detalleDocumentosEnviados;
	}

	public List<DetalleAnexoCorrespondencia> getDetalleAnexoCorrespondencias() {
		return detalleAnexoCorrespondencias;
	}

	public void setDetalleAnexoCorrespondencias(List<DetalleAnexoCorrespondencia> detalleAnexoCorrespondencias) {
		this.detalleAnexoCorrespondencias = detalleAnexoCorrespondencias;
	}

	public List<DetalleObservacionesDocumento> getDetalleObseravacionesDocumento() {
		return detalleObseravacionesDocumento;
	}

	public void setDetalleObseravacionesDocumento(List<DetalleObservacionesDocumento> detalleObseravacionesDocumento) {
		this.detalleObseravacionesDocumento = detalleObseravacionesDocumento;
	}

	public List<DetalleCopiasDocumento> getDetalleCopiasDocumentos() {
		return detalleCopiasDocumentos;
	}

	public void setDetalleCopiasDocumentos(List<DetalleCopiasDocumento> detalleCopiasDocumentos) {
		this.detalleCopiasDocumentos = detalleCopiasDocumentos;
	}

}