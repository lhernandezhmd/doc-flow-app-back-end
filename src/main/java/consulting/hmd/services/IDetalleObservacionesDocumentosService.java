package consulting.hmd.services;

import consulting.hmd.model.DetalleObservacionesDocumento;
import consulting.hmd.services.common.IServices;


public interface IDetalleObservacionesDocumentosService
		extends IServices<DetalleObservacionesDocumento, Integer> {

}
