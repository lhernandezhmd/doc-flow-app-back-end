package consulting.hmd.services.common;

import java.util.List;

import consulting.hmd.exceptions.ServiceException;

public interface IServices<T, ID> {

	Integer Crear(T ob) throws ServiceException;

	Integer Modificar(T ob) throws ServiceException;

	Integer Eliminar(ID id) throws ServiceException;

	T ObtenerPorId(ID id) throws Exception;

	List<T> Obtener() throws Exception;

	void setUsuario(String usuario);

}
