package consulting.hmd.services.seg;

import consulting.hmd.model.seg.ResetToken;

public interface IResetTokenService {

	ResetToken findByToken(String token);

	void guardar(ResetToken token);

	void eliminar(ResetToken token);
}
