package consulting.hmd.services;

import consulting.hmd.model.DetalleDocumentosRecibidos;
import consulting.hmd.services.common.IServices;


public interface IDetalleDocumentosRecibidosService extends IServices<DetalleDocumentosRecibidos, Integer> {

}
