package consulting.hmd.services.seg;

import consulting.hmd.model.seg.Rol;
import consulting.hmd.services.common.IServices;

public interface IRolService extends IServices<Rol, Integer> {

}
