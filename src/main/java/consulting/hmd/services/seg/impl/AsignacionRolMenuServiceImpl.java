package consulting.hmd.services.seg.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.AsignacionRolesMenuPrincipal;
import consulting.hmd.repository.seg.IAsignacionRolMenuPrincipalRepo;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.seg.IAsignacionRolesMenuService;

@Service
public class AsignacionRolMenuServiceImpl extends InicializarService<AsignacionRolesMenuPrincipal, Integer>
		implements IAsignacionRolesMenuService {

	@Autowired
	IAsignacionRolMenuPrincipalRepo repo;
	private String usuario;

	public AsignacionRolMenuServiceImpl() {
	}

	@Override
	public Integer Crear(AsignacionRolesMenuPrincipal ob) throws ServiceException {
		ob.setFecha_creacion(LocalDateTime.now());
		ob.setUsuarioCreacion(getUsuario());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(AsignacionRolesMenuPrincipal ob) throws ServiceException {
		AsignacionRolesMenuPrincipal base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con ese Id");

		base.setMenuPrincipal(ob.getMenuPrincipal());
		base.setRol(ob.getRol());

		base.setUsuarioModificacion(getUsuario());
		base.setFecha_modificacion(LocalDateTime.now());

		return serv.Salvar(base, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public AsignacionRolesMenuPrincipal ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<AsignacionRolesMenuPrincipal> Obtener() throws Exception {
		return serv.Obtener();
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}
}