package consulting.hmd.controller.common;

import java.io.Serializable;
import java.util.List;

public class Salida<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private int respuesta;
	private List<T>listado;
	private T tipo;
	private String salida;
	private Error error;

	public int getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}
	public List<T> getListado() {
		return listado;
	}
	public void setListado(List<T> listado) {
		this.listado = listado;
	}
	public T getTipo() {
		return tipo;
	}
	public void setTipo(T tipo) {
		this.tipo = tipo;
	}
	public String getSalida() {
		return salida;
	}
	public void setSalida(String salida) {
		this.salida = salida;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
}
