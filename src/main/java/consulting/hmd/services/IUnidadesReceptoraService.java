package consulting.hmd.services;



import consulting.hmd.model.UnidadesReceptoras;
import consulting.hmd.services.common.IServices;


public interface IUnidadesReceptoraService extends IServices<UnidadesReceptoras, Integer> {

}
