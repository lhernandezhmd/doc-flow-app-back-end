package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;

import consulting.hmd.model.seg.Rol;


public interface IRolRepository extends JpaRepository<Rol, Integer> {

}
