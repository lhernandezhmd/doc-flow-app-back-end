package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.CorrespondenciaRecibida;

@Repository
public interface ICorrespondenciaRecibidaRepository extends JpaRepository<CorrespondenciaRecibida, Integer> {

}
