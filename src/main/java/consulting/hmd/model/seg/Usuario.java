package consulting.hmd.model.seg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.hmd.model.common.EntityBase;


@Entity
@Table(name = "seg_usuarios", schema = "public")
public class Usuario extends EntityBase<Integer> implements UserDetails {

	private static final long serialVersionUID = 1L;

	public Usuario() {
	}

	public Usuario(int id) {
		setId(id);
	}

	@Column(name = "nombre_usuario", nullable = false, unique = true)
	private String username;

	@Column(name = "nombre_completo_usuario", nullable = false, unique = true)
	private String nombre_completo_usuario;

	@Column(name = "correo_electronico", nullable = false, unique = true)
	private String correo_electronico;

	@JsonIgnore
	@Column(name = "contrasena", nullable = false)
	private String password;

	@Column(name = "estado", nullable = false)
	private boolean enabled;

	@Column(name = "foto", nullable = true, length = 2000)
	private String foto;

	@Transient
	private String rol;

	@JsonIgnore
	@OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
	private List<AsignacionRolesUsuarios> rolesAsignadosUsuarios;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		getRolesAsignadosUsuarios()
				.forEach(rol -> authorities.add(new SimpleGrantedAuthority(rol.getRol().getNombre())));

		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return this.enabled;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public List<AsignacionRolesUsuarios> getRolesAsignadosUsuarios() {
		return rolesAsignadosUsuarios;
	}

	public void setRolesAsignadosUsuarios(List<AsignacionRolesUsuarios> rolesAsignadosUsuarios) {
		this.rolesAsignadosUsuarios = rolesAsignadosUsuarios;
	}

	public String getNombre_completo_usuario() {
		return nombre_completo_usuario;
	}

	public void setNombre_completo_usuario(String nombre_completo_usuario) {
		this.nombre_completo_usuario = nombre_completo_usuario;
	}

	public String getCorreo_electronico() {
		return correo_electronico;
	}

	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getRol() {
		
		try {
			if (getRolesAsignadosUsuarios().get(0)==null)
				return "";
			return getRolesAsignadosUsuarios().get(0).getRol().getNombre();	
		} catch (Exception e) {
			// TODO: handle exception
			return "[Debe asignar Un Rol al Usuario]";
		}
		
	}

}