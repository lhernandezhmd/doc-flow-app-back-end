package consulting.hmd.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

//import org.apache.commons.io.FilenameUtils;

public class Archivos {

	public Archivos() throws Exception {
		ObtenerPropiedades();
	}

	private String DirectorioImagen;
	private String ImagenNoEncontrada;
	private String ImagenUrlBase;

	protected void ObtenerPropiedades() throws Exception {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			String filename = "/propiedades.properties";
			input = Archivos.class.getResourceAsStream(filename);
			if (input == null) {
				throw new Exception("no se pueden cargar las Propiedades");
			}

			prop.load(input);

			this.DirectorioImagen = prop.getProperty("DirectorioImagen");
			this.ImagenNoEncontrada = prop.getProperty("ImagenNoEncontrada");
			this.ImagenUrlBase = prop.getProperty("ImagenUrlBase");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
//	public String ObtenerExtencion(String archivo) 
//	{
//		return FilenameUtils.getExtension(archivo);
//	}


	public String CrearArchivo(String extencion, InputStream input, String direccion, String nombreArchivoSinExtencion)
			throws Exception {
		String nombreArchivo = nombreArchivoSinExtencion + "." + extencion;
		String archivo = direccion + nombreArchivo;
		File file = new File(archivo);
		if (file.exists()) {
			file.delete();
		}
		try {
			Files.copy(input, file.toPath());
			return "OK";
		} catch (IOException e) {

			return "Erro al Subir los Archivos" + e.getMessage() + " " + e.getCause();

		}
	}

	public String getDirectorioImagen() {
		return DirectorioImagen;
	}

	public void setDirectorioImagen(String directorioImagen) {
		DirectorioImagen = directorioImagen;
	}

	public String getImagenNoEncontrada() {
		return ImagenNoEncontrada;
	}

	public void setImagenNoEncontrada(String imagenNoEncontrada) {
		ImagenNoEncontrada = imagenNoEncontrada;
	}

	public String getImagenUrlBase() {
		return ImagenUrlBase;
	}

	public void setImagenUrlBase(String imagenUrlBase) {
		ImagenUrlBase = imagenUrlBase;
	}

}
