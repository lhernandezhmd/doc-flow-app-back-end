package consulting.hmd.repository.seg.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import consulting.hmd.model.seg.menu.MenuBase;
import consulting.hmd.repository.seg.IMenuRepository;

@Component
@Transactional
public class MenuRepoImpl implements IMenuRepository {

	@Value("${spring.datasource.url}")
	private String urlDb;

	@Value("${spring.datasource.username}")
	private String usuario;

	@Value("${spring.datasource.password}")
	private String password;

	@Value("${spring.datasource.driver-class-name}")
	private String urlDriver;

	private String SQL;

	@Override
	public List<MenuBase> ObtenerMenuPorRol(Integer IdRol) throws SQLException, ClassNotFoundException {
		try {

			Class.forName(urlDriver);
			Connection connection = null;
			connection = DriverManager.getConnection(urlDb, usuario, password);

			List<MenuBase> listado = new ArrayList<>();

			SQL = "with recursive MENU as(\r\n" + "	select\r\n" + "		smp.id,\r\n" + "		smp.icono,\r\n"
					+ "		smp.titulo,\r\n" + "		smp.url,\r\n" + "		smp.id_menu_padre,\r\n"
					+ "		smp.estado\r\n" + "	from\r\n" + "		seg_menu_principal smp\r\n" + "union select\r\n"
					+ "		m.id,\r\n" + "		m.icono,\r\n" + "		m.titulo,\r\n" + "		m.url,\r\n"
					+ "		m.id_menu_padre,\r\n" + "		m.estado\r\n" + "	from\r\n"
					+ "		seg_menu_principal m\r\n" + "	inner join MENU MN on\r\n"
					+ "		MN.id = m.id_menu_padre\r\n" + ") select\r\n" + "	men.*\r\n" + "from\r\n"
					+ "	MENU men\r\n" + "inner join seg_rol_menu_principal srmp on\r\n"
					+ "	srmp.id_menu_principal = men.id\r\n" + "where\r\n" + "	srmp.id_rol=?";

			PreparedStatement stmt = connection.prepareStatement(SQL);
			stmt.setInt(1, IdRol);

			ResultSet rs = stmt.executeQuery();

			MenuBase item = new MenuBase();

			while (rs.next()) {

				item = new MenuBase(rs.getInt("id"), rs.getString("titulo"), rs.getString("icono"), rs.getString("url"),
						rs.getInt("id_menu_padre"), rs.getBoolean("estado"));
				listado.add(item);
			}

			rs.close();
			stmt.close();
			connection.close();

			return listado;
		} catch (SQLException e) {
			throw new SQLException(e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException(e.getMessage());
		}
	}

}