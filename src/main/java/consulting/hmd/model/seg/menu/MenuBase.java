package consulting.hmd.model.seg.menu;

import java.io.Serializable;

public class MenuBase implements Serializable {

	private static final long serialVersionUID = 1L;

	public MenuBase() {
	}

	public MenuBase(int id) {
		this.id = id;
	}

	public MenuBase(Integer id, String titulo, String icono, String url, Integer idPadre) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.icono = icono;
		this.url = url;
		this.idPadre = idPadre;
	}

	public MenuBase(Integer id, String titulo, String icono, String url, Integer idPadre, boolean estado) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.icono = icono;
		this.url = url;
		this.idPadre = idPadre;
		this.estado = estado;
	}

	private Integer id;
	private String titulo;
	private String icono;
	private String url;
	private Integer idPadre;
	private boolean estado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(Integer idPadre) {
		this.idPadre = idPadre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
