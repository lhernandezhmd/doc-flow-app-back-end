package consulting.hmd.services.seg.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.Rol;
import consulting.hmd.repository.seg.IRolRepo;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;
import consulting.hmd.services.seg.IRolService;

@Service
public class RolServiceImpl extends InicializarService<Rol, Integer> implements IRolService {

	@Autowired
	IRolRepo repo;
	private String usuario;

	public RolServiceImpl() {
		serv = new ServiceImpl<Rol, Integer>(repo);
		base = new Rol();
	}

	@Override
	public Integer Crear(Rol ob) throws ServiceException {
		ob.setUsuarioCreacion(this.getUsuario());
		ob.setFecha_creacion(LocalDateTime.now());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(Rol ob) throws ServiceException {

		base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con ese Id");

		base.setNombre(ob.getNombre());
		base.setDescripcion(ob.getDescripcion());
		base.setUsuarioModificacion(getUsuario());
		base.setFecha_modificacion(LocalDateTime.now());
		
		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public Rol ObtenerPorId(Integer id) throws Exception {
		return repo.findById(id).get();
	}

	@Override
	public List<Rol> Obtener() throws Exception {
		return repo.findAll();
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}
}