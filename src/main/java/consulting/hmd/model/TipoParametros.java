package consulting.hmd.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import consulting.hmd.model.common.EntityBase;

public class TipoParametros extends EntityBase<Integer>{

	private static final long serialVersionUID = 1L;

	public TipoParametros(int id) {
		setId(id);
	}

	public TipoParametros() {
	}

	@Column(name = "tipo_parametro", nullable = false, length = 30)
	private String tipoParametro;

	@Column(name = "descripcion", nullable = false, length = 2000)
	private String descripcion;
	
	private int orden;

	@OneToMany(mappedBy = "tipoParametro", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY)
	private List<Parametros> parametros;

	public String getTipoParametro() {
		return tipoParametro;
	}

	public void setTipoParametro(String tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Parametros> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametros> parametros) {
		this.parametros = parametros;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}
}
