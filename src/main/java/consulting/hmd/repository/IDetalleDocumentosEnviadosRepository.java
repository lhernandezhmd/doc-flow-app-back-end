package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleDocumentosEnviados;

@Repository
public interface IDetalleDocumentosEnviadosRepository extends JpaRepository<DetalleDocumentosEnviados, Integer> {

}
