package consulting.hmd.services;

import consulting.hmd.model.seg.AsignacionRolesUsuarios;
import consulting.hmd.services.common.IServices;

public interface IAsignacionRolesUsuariosService extends IServices<AsignacionRolesUsuarios, Integer> {

}
