package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.AsignacionRolesUsuarios;

@Repository
public interface IAsignacionRolesUsuariosRepository extends JpaRepository<AsignacionRolesUsuarios, Integer> {

}
