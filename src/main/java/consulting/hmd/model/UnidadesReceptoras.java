package consulting.hmd.model;

import java.util.List;

import consulting.hmd.model.common.EntityBase;

public class UnidadesReceptoras extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private String nombre;
	private String telefono;
	private List<CorrespondenciaRecibida> correspondenciasRecibidas;

	public UnidadesReceptoras(int id) {
		setId(id);
	}
	
	public UnidadesReceptoras() {
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<CorrespondenciaRecibida> getCorrespondenciasRecibidas() {
		return correspondenciasRecibidas;
	}

	public void setCorrespondenciasRecibidas(List<CorrespondenciaRecibida> correspondenciasRecibidas) {
		this.correspondenciasRecibidas = correspondenciasRecibidas;
	}

}