package consulting.hmd.services.seg.impl;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.MenuPrincipal;
import consulting.hmd.model.seg.menu.MenuBase;
import consulting.hmd.model.seg.menu.MenuGeneral;
import consulting.hmd.model.seg.menu.SubMenu;
import consulting.hmd.repository.seg.IMenuPrincipalRepository;
import consulting.hmd.repository.seg.IMenuRepository;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;
import consulting.hmd.services.seg.IMenuService;

@Service
public class MenuServiceImpl extends InicializarService<MenuPrincipal, Integer> implements IMenuService {

	@Autowired
	IMenuPrincipalRepository repo;

	@Autowired
	IMenuRepository menuRepo;
	private String usuario;

	public MenuServiceImpl() {
		serv = new ServiceImpl<MenuPrincipal, Integer>(repo);
	}

	@Override
	public Integer Crear(MenuPrincipal ob) throws ServiceException {
		ob.setUsuarioCreacion(this.getUsuario());
		ob.setFecha_creacion(LocalDateTime.now());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(MenuPrincipal ob) throws ServiceException {
		MenuPrincipal base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con este Id");

		base.setTitulo(ob.getTitulo());
		base.setUrl(ob.getUrl());
		base.setIcono(ob.getIcono());
		base.setUsuarioModificacion(getUsuario());
		base.setFecha_modificacion(LocalDateTime.now());

		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public MenuPrincipal ObtenerPorId(Integer id) throws Exception {
		return repo.findById(id).get();
	}

	@Override
	public List<MenuPrincipal> Obtener() throws Exception {
		return repo.findAll();
	}

	@Override
	public void setUsuario(String usuario) {
		// TODO Auto-generated method stub
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	@Override
	public List<MenuGeneral> ObtenerMenuPorRol(Integer IdRol) throws SQLException, ClassNotFoundException {

		try {
			List<MenuBase> menuBase = menuRepo.ObtenerMenuPorRol(IdRol);
			List<MenuGeneral> menuPrincipal = new ArrayList<>();

			MenuGeneral menu = new MenuGeneral();

			List<MenuBase> elementos = new ArrayList<>();
			List<SubMenu> subMenus = new ArrayList<>();
			List<MenuBase> menuAgregados = new ArrayList<>();
			SubMenu subMenu = new SubMenu();

			if (menuBase == null)
				return null;

			for (MenuBase men : menuBase) {

				elementos = new ArrayList<>();

				elementos = menuBase.stream().filter(p -> p.getIdPadre() == men.getId()).collect(Collectors.toList());

				if (!existente(men.getId(), menuAgregados)) {
					if (elementos.size() > 0) {
						menu = new MenuGeneral();
						menu.setId(men.getId());
						menu.setIcono(men.getIcono());
						menu.setUrl(men.getUrl());
						menu.setEstado(men.isEstado());
						menu.setTitulo(men.getTitulo());

						subMenus = new ArrayList<>();
						for (MenuBase elemento : elementos) {
							subMenu = new SubMenu();
							subMenu.setId(elemento.getId());
							subMenu.setIcono(elemento.getIcono());
							subMenu.setUrl(elemento.getUrl());
							subMenu.setEstado(elemento.isEstado());
							subMenu.setTitulo(elemento.getTitulo());
							subMenus.add(subMenu);
							menu.setSubMenus(subMenus);
							menuAgregados.add(elemento);
						}
					} else {
						menu = new MenuGeneral();
						menu.setId(men.getId());
						menu.setIcono(men.getIcono());
						menu.setUrl(men.getUrl());
						menu.setEstado(men.isEstado());
						menu.setTitulo(men.getTitulo());
						menu.setSubMenus(null);
					}
					menuPrincipal.add(menu);
				}
			}

			return menuPrincipal;
		} catch (SQLException e) {
			throw new SQLException(e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new ClassNotFoundException(e.getMessage());
		}
	}

	private boolean existente(int id, List<MenuBase> menu) {
		try {
			List<MenuBase> existente = menu.stream().filter(p -> p.getId() == id).collect(Collectors.toList());

			if (existente.size() > 0)
				return true;

			return false;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}
}