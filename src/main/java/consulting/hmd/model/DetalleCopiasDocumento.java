package consulting.hmd.model;

import consulting.hmd.model.common.EntityBase;

public class DetalleCopiasDocumento extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private CorrespondenciaRecibida correspondenciaRecibida;
	private int idEmpleadoEnviaCC;
	private int idEmpladoRecibeCC;
	private int idDetalleCorrespondenciaRecibidaOriginal;

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public int getIdEmpleadoEnviaCC() {
		return idEmpleadoEnviaCC;
	}

	public void setIdEmpleadoEnviaCC(int idEmpleadoEnviaCC) {
		this.idEmpleadoEnviaCC = idEmpleadoEnviaCC;
	}

	public int getIdEmpladoRecibeCC() {
		return idEmpladoRecibeCC;
	}

	public void setIdEmpladoRecibeCC(int idEmpladoRecibeCC) {
		this.idEmpladoRecibeCC = idEmpladoRecibeCC;
	}

	public int getIdDetalleCorrespondenciaRecibidaOriginal() {
		return idDetalleCorrespondenciaRecibidaOriginal;
	}

	public void setIdDetalleCorrespondenciaRecibidaOriginal(int idDetalleCorrespondenciaRecibidaOriginal) {
		this.idDetalleCorrespondenciaRecibidaOriginal = idDetalleCorrespondenciaRecibidaOriginal;
	}

}
