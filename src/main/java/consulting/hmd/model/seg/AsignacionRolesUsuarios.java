package consulting.hmd.model.seg;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.hmd.model.common.EntityBase;


@Entity
@Table(name = "seg_roles_usuarios", schema = "public")
public class AsignacionRolesUsuarios extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	public AsignacionRolesUsuarios() {
	}

	public AsignacionRolesUsuarios(int id) {
		setId(id);
	}

	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	@JsonIgnore
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "id_rol", nullable = false)
	@JsonIgnore
	private Rol rol;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
}
