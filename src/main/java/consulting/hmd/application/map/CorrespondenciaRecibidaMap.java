package consulting.hmd.application.map;

import java.time.LocalDateTime;

import consulting.hmd.application.dto.CorrespondenciaRecibidaDto;
import consulting.hmd.model.CorrespondenciaRecibida;
import consulting.hmd.model.Parametros;
import consulting.hmd.model.UnidadesEmisoras;
import consulting.hmd.model.UnidadesReceptoras;
import consulting.hmd.util.DateConverter;

public class CorrespondenciaRecibidaMap {

	public static CorrespondenciaRecibida ToModel(CorrespondenciaRecibidaDto dto) throws Exception {
		CorrespondenciaRecibida model = new CorrespondenciaRecibida();
		model.setId(dto.getId());
		model.setTipoDocumento(new Parametros(dto.getIdTipoDocumento()));
		model.setTipoCorrespondencia(new Parametros(dto.getIdTipoCorrespondencia()));
		model.setUnidadEmisora(new UnidadesEmisoras(dto.getIdUnidadEmisora()));
		model.setUnidadReceptora(new UnidadesReceptoras(dto.getIdUnidadReceptora()));
		model.setNumeroDocumento(dto.getNumeroDocumento());
		model.setPersonaRemitente(dto.getPersonaRemitente());
		model.setFechaDocumento(
				DateConverter.ToLocalDate(DateConverter.FormatoFecha(dto.getFechaDocumento()), "yyyyMMdd"));
		model.setFechaRecepcion(
				DateConverter.ToLocalDate(DateConverter.FormatoFecha(dto.getFechaDocumento()), "yyyyMMdd"));
		model.setFechaIngreso(LocalDateTime.now());
		model.setExtencionArchivoImagen(dto.getExtencionArchivoImagen());
		model.setUrlPdf(dto.getUrlPdf());
		return model;
	}
}
