package consulting.hmd.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.AsignacionRolesUsuarios;
import consulting.hmd.repository.IAsignacionRolesUsuariosRepository;
import consulting.hmd.services.IAsignacionRolesUsuariosService;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;

@Service
public class AsignacionRolesUsuariosService extends InicializarService<AsignacionRolesUsuarios, Integer>
		implements IAsignacionRolesUsuariosService {

	@Autowired
	private IAsignacionRolesUsuariosRepository repo;
	private String usuario;

	public AsignacionRolesUsuariosService() {
		serv = new ServiceImpl<>(repo);
		base = new AsignacionRolesUsuarios();
	}

	@Override
	public Integer Crear(AsignacionRolesUsuarios ob) throws ServiceException {
		ob.setFecha_creacion(LocalDateTime.now());
		ob.setUsuarioCreacion(getUsuario());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(AsignacionRolesUsuarios ob) throws ServiceException {

		base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con este Id");

		base.setUsuario(ob.getUsuario());
		base.setRol(ob.getRol());
		base.setFecha_modificacion(LocalDateTime.now());
		base.setUsuarioModificacion(getUsuario());

		return serv.Salvar(base, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public AsignacionRolesUsuarios ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<AsignacionRolesUsuarios> Obtener() throws Exception {
		return serv.Obtener();
	}

	public String getUsuario() {
		return usuario;
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
}