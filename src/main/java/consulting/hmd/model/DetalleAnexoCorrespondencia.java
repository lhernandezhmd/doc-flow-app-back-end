package consulting.hmd.model;

import java.time.LocalDateTime;

import consulting.hmd.model.common.EntityBase;

public class DetalleAnexoCorrespondencia extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private CorrespondenciaRecibida correspondenciaRecibida;
	private Empleado empleado;
	private String nombreDocumento;
	private LocalDateTime fechaAnexacion;

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public LocalDateTime getFechaAnexacion() {
		return fechaAnexacion;
	}

	public void setFechaAnexacion(LocalDateTime fechaAnexacion) {
		this.fechaAnexacion = fechaAnexacion;
	}

}