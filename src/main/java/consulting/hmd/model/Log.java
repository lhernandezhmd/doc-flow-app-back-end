package consulting.hmd.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bit_transacciones" , schema="public")
public class Log  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;

	@Column(name = "id_registro", nullable = false)
	private Integer IdRegistro;

	@Column(name = "actividad", nullable = false, length = 25)
	private String actividad;

	@Column(name = "descripcion_actividad", nullable = false, length = 200)
	private String descripcionActividad;

	@Column(name = "nombre_tabla", nullable = false, length = 30)
	private String nombreTabla;

	@Column(name = "nombre_modulo", nullable = false, length = 30)
	private String nombreModulo;

	@Column(name = "nombre_pantalla", nullable = false, length = 30)
	private String nombrePantalla;

	@Column(name = "nombre_metodo", nullable = false, length = 50)
	private String nombreMetodo;

	@Column(name = "fecha_actividad", nullable = false)
	private LocalDateTime fechaActividad;

	@Column(name = "usuario_actividad", nullable = false, length = 100)
	private String usuarioActividad;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getIdRegistro() {
		return IdRegistro;
	}

	public void setIdRegistro(Integer idRegistro) {
		IdRegistro = idRegistro;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public String getDescripcionActividad() {
		return descripcionActividad;
	}

	public void setDescripcionActividad(String descripcionActividad) {
		this.descripcionActividad = descripcionActividad;
	}

	public String getNombreTabla() {
		return nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}

	public String getNombreModulo() {
		return nombreModulo;
	}

	public void setNombreModulo(String nombreModulo) {
		this.nombreModulo = nombreModulo;
	}

	public String getNombrePantalla() {
		return nombrePantalla;
	}

	public void setNombrePantalla(String nombrePantalla) {
		this.nombrePantalla = nombrePantalla;
	}

	public LocalDateTime getFechaActividad() {
		return fechaActividad;
	}

	public void setFechaActividad(LocalDateTime fechaActividad) {
		this.fechaActividad = fechaActividad;
	}

	public String getUsuarioActividad() {
		return usuarioActividad;
	}

	public void setUsuarioActividad(String usuarioActividad) {
		this.usuarioActividad = usuarioActividad;
	}

	public String getNombreMetodo() {
		return nombreMetodo;
	}

	public void setNombreMetodo(String nombreMetodo) {
		this.nombreMetodo = nombreMetodo;
	}

}