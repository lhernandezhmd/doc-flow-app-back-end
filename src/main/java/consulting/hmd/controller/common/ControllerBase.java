package consulting.hmd.controller.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class ControllerBase<T> {

	public List<T> listado=new ArrayList<>();
	public T entidad;
	public Integer codigo;

	public String UsuarioLogeado() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String usuario = auth.getPrincipal().toString();
		return usuario;
	};
}