package consulting.hmd.model;

import java.time.LocalDateTime;

import consulting.hmd.model.common.EntityBase;

public class DetalleActividadesDocumentoEmpleados extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;
	
	private CorrespondenciaRecibida correspondenciaRecibida;
	private DetalleDocumentosRecibidos detalleDocumentoRecibido;
	private Empleado empleado;
	private LocalDateTime fechaActividad;
	private String actividad;

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public DetalleDocumentosRecibidos getDetalleDocumentoRecibido() {
		return detalleDocumentoRecibido;
	}

	public void setDetalleDocumentoRecibido(DetalleDocumentosRecibidos detalleDocumentoRecibido) {
		this.detalleDocumentoRecibido = detalleDocumentoRecibido;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public LocalDateTime getFechaActividad() {
		return fechaActividad;
	}

	public void setFechaActividad(LocalDateTime fechaActividad) {
		this.fechaActividad = fechaActividad;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

}
