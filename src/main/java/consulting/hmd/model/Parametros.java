package consulting.hmd.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.hmd.model.common.EntityBase;

public class Parametros extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	@Column(name = "codigo", nullable = false, length = 10)
	private String codigo;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "id_tipo_parametro", nullable = false)
	private TipoParametros tipoParametro;
	
	public Parametros(int id) {
		setId(id);
	}
	
	public Parametros() {
	}

	@Column(name = "descripcion", nullable = false, length = 2000)
	private String descripcion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public TipoParametros getTipoParametro() {
		return tipoParametro;
	}

	public void setTipoParametro(TipoParametros tipoParametro) {
		this.tipoParametro = tipoParametro;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}