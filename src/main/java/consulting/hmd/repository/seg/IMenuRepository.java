package consulting.hmd.repository.seg;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Component;

import consulting.hmd.model.seg.menu.MenuBase;


@Component
public interface IMenuRepository {
	List<MenuBase> ObtenerMenuPorRol(Integer IdRol) throws SQLException,ClassNotFoundException;
}
