package consulting.hmd.services;


import consulting.hmd.model.TipoParametros;
import consulting.hmd.services.common.IServices;


public interface ITipoParametroService extends IServices<TipoParametros, Integer> {

}
