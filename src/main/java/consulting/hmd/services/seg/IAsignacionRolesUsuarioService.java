package consulting.hmd.services.seg;

import consulting.hmd.model.seg.AsignacionRolesUsuarios;
import consulting.hmd.services.common.IServices;

public interface IAsignacionRolesUsuarioService extends IServices<AsignacionRolesUsuarios, Integer> {

}
