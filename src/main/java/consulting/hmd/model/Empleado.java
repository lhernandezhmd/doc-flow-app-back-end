package consulting.hmd.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import consulting.hmd.model.common.EntityBase;

public class Empleado extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "identidad", nullable = false, length = 20)
	private String identidad;

	@Column(name = "primer_nombre", nullable = false, length = 25)
	private String primerNombre;

	@Column(name = "segundo_nombre", nullable = false, length = 25)
	private String segundoNombre;

	@Column(name = "primer_apellido", nullable = false, length = 25)
	private String primerApellido;

	@Column(name = "segundo_apellido", nullable = false, length = 25)
	private String segundoApellido;

	@Column(name = "foto", nullable = false, length = 2000)
	private String foto;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDate fechaNacimiento;

	@Column(name = "correo_electronico", nullable = false, length = 100)
	private String correoElectronico;
	
	@Column(name = "direccion", nullable = false, length = 2000)
	private String direccion;

	@Column(name = "telefono_celular", nullable = false, length = 10)
	private String telefonoCelular;

	@Column(name = "extencion", nullable = false, length = 10)
	private String extencion;

	@Column(name = "telefono_casa", nullable = false, length = 10)
	private String telefonoCasa;

	@ManyToOne
	@JoinColumn(name = "id_puesto", nullable = false)
	private Parametros puesto;
	
	@ManyToOne
	@JoinColumn(name = "id_sexo", nullable = false)
	private Parametros sexo;

	public String getIdentidad() {
		return identidad;
	}

	public void setIdentidad(String identidad) {
		this.identidad = identidad;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getExtencion() {
		return extencion;
	}

	public void setExtencion(String extencion) {
		this.extencion = extencion;
	}

	public String getTelefonoCasa() {
		return telefonoCasa;
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public Parametros getPuesto() {
		return puesto;
	}

	public void setPuesto(Parametros puesto) {
		this.puesto = puesto;
	}

	public Parametros getSexo() {
		return sexo;
	}

	public void setSexo(Parametros sexo) {
		this.sexo = sexo;
	}
	
}