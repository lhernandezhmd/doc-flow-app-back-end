package consulting.hmd.controller.common;

import java.io.Serializable;

public class Error implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Error() {
	}

	public Error(int codigo, String excepcion) {
		super();
		this.codigo = codigo;
		this.excepcion = excepcion;
	}

	private int codigo;
	private String excepcion;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

}
