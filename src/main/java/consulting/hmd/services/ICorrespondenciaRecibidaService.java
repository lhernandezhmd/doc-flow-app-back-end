package consulting.hmd.services;


import consulting.hmd.model.CorrespondenciaRecibida;
import consulting.hmd.services.common.IServices;


public interface ICorrespondenciaRecibidaService extends IServices<CorrespondenciaRecibida, Integer> {

}
