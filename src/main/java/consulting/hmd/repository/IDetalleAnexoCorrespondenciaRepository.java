package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleAnexoCorrespondencia;

@Repository
public interface IDetalleAnexoCorrespondenciaRepository extends JpaRepository<DetalleAnexoCorrespondencia, Integer> {

}
