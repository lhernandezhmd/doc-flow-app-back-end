package consulting.hmd.services.seg.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.Usuario;
import consulting.hmd.repository.seg.IUserRepo;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;
import consulting.hmd.services.seg.IUsuarioService;

@Service
public class UsuarioServiceImpl extends InicializarService<Usuario, Integer> implements IUsuarioService {

	@Autowired
	private IUserRepo repo;
	private String usuario;
	private String Entidad = "Usuario";

	@Value("${contrasena.default}")
	private String contrasenaInicial;

	@SuppressWarnings("unused")
	private BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	public UsuarioServiceImpl() {
		serv = new ServiceImpl<>(repo);
		base = new Usuario();
	}

	@Override
	public Integer Crear(Usuario ob) throws ServiceException {
		ob.setFecha_creacion(LocalDateTime.now());
		ob.setUsuarioCreacion(getUsuario());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(Usuario ob) throws ServiceException {
		base = repo.findById(ob.getId()).get();
		if (base == null)
			throw new ServiceException(204, "No hay ningun " + Entidad + " con este Id:" + ob.getId());

		base.setNombre_completo_usuario(ob.getNombre_completo_usuario());
		base.setUsername(ob.getUsername());
		base.setCorreo_electronico(ob.getCorreo_electronico());
		base.setEnabled(ob.isEnabled());

		base.setUsuarioModificacion(getUsuario());
		base.setFecha_modificacion(LocalDateTime.now());

		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public Usuario ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<Usuario> Obtener() throws Exception {
		return serv.Obtener();
	}

	@Override
	public void setUsuario(String usuario) {
		// TODO Auto-generated method stub
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

}
