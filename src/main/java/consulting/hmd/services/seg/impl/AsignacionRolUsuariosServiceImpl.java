package consulting.hmd.services.seg.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.seg.AsignacionRolesUsuarios;
import consulting.hmd.repository.seg.IAsignacionRolesUsuariosRepo;
import consulting.hmd.services.IAsignacionRolesUsuariosService;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;

@Service
public class AsignacionRolUsuariosServiceImpl extends InicializarService<AsignacionRolesUsuarios, Integer>
		implements IAsignacionRolesUsuariosService {

	@Autowired
	IAsignacionRolesUsuariosRepo repo;

	private String usuario;

	public AsignacionRolUsuariosServiceImpl() {
		serv = new ServiceImpl<AsignacionRolesUsuarios, Integer>(repo);
	}

	@Override
	public Integer Crear(AsignacionRolesUsuarios ob) throws ServiceException {
		ob.setUsuarioCreacion(this.getUsuario());
		ob.setFecha_creacion(LocalDateTime.now());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(AsignacionRolesUsuarios ob) throws ServiceException {

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con este Id");

		base.setUsuario(ob.getUsuario());
		base.setRol(ob.getRol());

		base.setUsuarioModificacion(getUsuario());
		base.setFecha_modificacion(LocalDateTime.now());

		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public AsignacionRolesUsuarios ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<AsignacionRolesUsuarios> Obtener() throws Exception {
		return serv.Obtener();
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}
}