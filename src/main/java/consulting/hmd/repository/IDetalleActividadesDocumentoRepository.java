package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.DetalleActividadesDocumentoEmpleados;

@Repository
public interface IDetalleActividadesDocumentoRepository
		extends JpaRepository<DetalleActividadesDocumentoEmpleados, Integer> {

}
