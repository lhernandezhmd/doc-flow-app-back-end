package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.AsignacionRolesMenuPrincipal;


@Repository
public interface IAsignacionMenuRolRepository extends JpaRepository<AsignacionRolesMenuPrincipal, Integer> {

}
