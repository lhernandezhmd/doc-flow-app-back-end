package consulting.hmd.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.TipoParametros;
import consulting.hmd.repository.ITipoParametroRepository;
import consulting.hmd.services.ITipoParametroService;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;

@Service
public class TipoParametroServiceImpl extends InicializarService<TipoParametros, Integer>
		implements ITipoParametroService {

	@Autowired
	private ITipoParametroRepository repo;
	private String usuario;

	public TipoParametroServiceImpl() {
		serv = new ServiceImpl<>(repo);
		base = new TipoParametros();
	}

	@Override
	public Integer Crear(TipoParametros ob) throws ServiceException {
		ob.setFecha_creacion(LocalDateTime.now());
		ob.setUsuarioCreacion(getUsuario());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(TipoParametros ob) throws ServiceException {
		base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con ese Id");

		base.setFecha_modificacion(LocalDateTime.now());
		base.setUsuarioModificacion(getUsuario());

		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public TipoParametros ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<TipoParametros> Obtener() throws Exception {
		return serv.Obtener();
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

}
