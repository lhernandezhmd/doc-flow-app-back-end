package consulting.hmd.services;


import consulting.hmd.model.DetalleActividadesDocumentoEmpleados;
import consulting.hmd.services.common.IServices;

public interface IDetalleActividadesDocumentoService
		extends IServices<DetalleActividadesDocumentoEmpleados, Integer> {

}
