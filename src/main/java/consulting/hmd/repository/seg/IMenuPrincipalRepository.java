package consulting.hmd.repository.seg;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.MenuPrincipal;


@Repository
public interface IMenuPrincipalRepository extends JpaRepository<MenuPrincipal, Integer> {

	@Query("SELECT a.menuPrincipal From AsignacionRolesMenuPrincipal a where a.rol.id=:p_id_rol")
	List<MenuPrincipal> ObtenerMenuPorRol(@Param("p_id_rol") Integer IdRol);
}
