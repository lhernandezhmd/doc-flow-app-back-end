package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.Parametros;

@Repository
public interface IParametroRepository extends JpaRepository<Parametros, Integer> {

}
