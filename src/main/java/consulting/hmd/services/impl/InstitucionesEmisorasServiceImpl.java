package consulting.hmd.services.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import consulting.hmd.exceptions.ServiceException;
import consulting.hmd.model.UnidadesEmisoras;
import consulting.hmd.repository.IInstitucionesEmisorasRepository;
import consulting.hmd.services.IInstitucionesEmisorasService;
import consulting.hmd.services.common.Acciones;
import consulting.hmd.services.common.InicializarService;
import consulting.hmd.services.common.ServiceImpl;

@Service
public class InstitucionesEmisorasServiceImpl extends InicializarService<UnidadesEmisoras, Integer>
		implements IInstitucionesEmisorasService {

	@Autowired
	private IInstitucionesEmisorasRepository repo;

	private String usuario;

	public InstitucionesEmisorasServiceImpl() {
		serv = new ServiceImpl<>(repo);
		base = new UnidadesEmisoras();
	}

	@Override
	public Integer Crear(UnidadesEmisoras ob) throws ServiceException {
		ob.setFecha_creacion(LocalDateTime.now());
		ob.setUsuarioCreacion(getUsuario());
		return serv.Salvar(ob, Acciones.CREAR);
	}

	@Override
	public Integer Modificar(UnidadesEmisoras ob) throws ServiceException {
		base = repo.findById(ob.getId()).get();

		if (base == null)
			throw new ServiceException(204, "No existe Ningun Registro con ese Id");

		base.setFecha_modificacion(LocalDateTime.now());
		base.setUsuarioModificacion(getUsuario());

		return serv.Salvar(ob, Acciones.MODIFICAR);
	}

	@Override
	public Integer Eliminar(Integer id) throws ServiceException {
		return serv.Eliminar(id);
	}

	@Override
	public UnidadesEmisoras ObtenerPorId(Integer id) throws Exception {
		return serv.ObtenerPorId(id);
	}

	@Override
	public List<UnidadesEmisoras> Obtener() throws Exception {
		return serv.Obtener();
	}

	@Override
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

}
