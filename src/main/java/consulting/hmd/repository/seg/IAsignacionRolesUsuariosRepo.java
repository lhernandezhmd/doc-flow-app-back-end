package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.seg.AsignacionRolesUsuarios;


@Repository
public interface IAsignacionRolesUsuariosRepo extends JpaRepository<AsignacionRolesUsuarios, Integer> {

}
