package consulting.hmd.services.seg;

import consulting.hmd.model.seg.AsignacionRolesMenuPrincipal;
import consulting.hmd.services.common.IServices;

public interface IAsignacionRolesMenuService extends IServices<AsignacionRolesMenuPrincipal, Integer> {

}