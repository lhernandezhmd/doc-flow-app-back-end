package consulting.hmd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import consulting.hmd.model.UnidadesReceptoras;

@Repository
public interface IUnidadesReceptoraRepository extends JpaRepository<UnidadesReceptoras, Integer> {

}
