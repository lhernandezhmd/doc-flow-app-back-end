package consulting.hmd.services.common;

import java.util.List;

import consulting.hmd.exceptions.ServiceException;

public interface IService<T, ID> {

	Integer Salvar(T ob, Acciones accion) throws ServiceException;

	Integer Eliminar(ID id) throws ServiceException;

	T ObtenerPorId(ID id) throws Exception;

	List<T> Obtener() throws Exception;
}
