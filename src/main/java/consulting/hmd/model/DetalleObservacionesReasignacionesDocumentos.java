package consulting.hmd.model;

import java.time.LocalDateTime;

import consulting.hmd.model.common.EntityBase;

public class DetalleObservacionesReasignacionesDocumentos extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	private CorrespondenciaRecibida correspondenciaRecibida;
	private DetalleDocumentosRecibidos detalleDocumentosRecibidos;
	private Empleado empleado;
	private LocalDateTime fecha;
	private String observaciones;

	public CorrespondenciaRecibida getCorrespondenciaRecibida() {
		return correspondenciaRecibida;
	}

	public void setCorrespondenciaRecibida(CorrespondenciaRecibida correspondenciaRecibida) {
		this.correspondenciaRecibida = correspondenciaRecibida;
	}

	public DetalleDocumentosRecibidos getDetalleDocumentosRecibidos() {
		return detalleDocumentosRecibidos;
	}

	public void setDetalleDocumentosRecibidos(DetalleDocumentosRecibidos detalleDocumentosRecibidos) {
		this.detalleDocumentosRecibidos = detalleDocumentosRecibidos;
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
}