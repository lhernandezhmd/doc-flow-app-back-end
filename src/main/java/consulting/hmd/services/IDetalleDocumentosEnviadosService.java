package consulting.hmd.services;

import consulting.hmd.model.DetalleDocumentosEnviados;
import consulting.hmd.services.common.IServices;


public interface IDetalleDocumentosEnviadosService extends IServices<DetalleDocumentosEnviados, Integer> {

}
