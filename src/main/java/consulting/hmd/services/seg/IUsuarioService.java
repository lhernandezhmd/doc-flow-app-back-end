package consulting.hmd.services.seg;

import consulting.hmd.model.seg.Usuario;
import consulting.hmd.services.common.IServices;

public interface IUsuarioService extends IServices<Usuario, Integer> {

}
