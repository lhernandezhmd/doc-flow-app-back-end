package consulting.hmd.services.seg;

import java.sql.SQLException;
import java.util.List;

import consulting.hmd.model.seg.MenuPrincipal;
import consulting.hmd.model.seg.menu.MenuGeneral;
import consulting.hmd.services.common.IServices;

public interface IMenuService extends IServices<MenuPrincipal, Integer> {

	List<MenuGeneral> ObtenerMenuPorRol(Integer IdRol) throws SQLException, ClassNotFoundException;
}
