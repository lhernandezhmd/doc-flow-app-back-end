package consulting.hmd.application.dto;

import java.io.Serializable;

public class CorrespondenciaRecibidaDto implements Serializable {

	private static final long serialVersionUID = 1L;
//	
	private int id;
	private int idTipoDocumento;
	private int idTipoCorrespondencia;
	private int idUnidadEmisora;
	private int idUnidadReceptora;
	private String numeroDocumento;
	private String personaRemitente;
	private String fechaDocumento;
	private String fechaRecepcion;

	private String extencionArchivoImagen;
	private String urlPdf;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdTipoDocumento() {
		return idTipoDocumento;
	}

	public void setIdTipoDocumento(int idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	public int getIdTipoCorrespondencia() {
		return idTipoCorrespondencia;
	}

	public void setIdTipoCorrespondencia(int idTipoCorrespondencia) {
		this.idTipoCorrespondencia = idTipoCorrespondencia;
	}

	public int getIdUnidadEmisora() {
		return idUnidadEmisora;
	}

	public void setIdUnidadEmisora(int idUnidadEmisora) {
		this.idUnidadEmisora = idUnidadEmisora;
	}

	public int getIdUnidadReceptora() {
		return idUnidadReceptora;
	}

	public void setIdUnidadReceptora(int idUnidadReceptora) {
		this.idUnidadReceptora = idUnidadReceptora;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getPersonaRemitente() {
		return personaRemitente;
	}

	public void setPersonaRemitente(String personaRemitente) {
		this.personaRemitente = personaRemitente;
	}

	public String getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getFechaRecepcion() {
		return fechaRecepcion;
	}

	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public String getExtencionArchivoImagen() {
		return extencionArchivoImagen;
	}

	public void setExtencionArchivoImagen(String extencionArchivoImagen) {
		this.extencionArchivoImagen = extencionArchivoImagen;
	}

	public String getUrlPdf() {
		return urlPdf;
	}

	public void setUrlPdf(String urlPdf) {
		this.urlPdf = urlPdf;
	}
}