package consulting.hmd.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import consulting.hmd.application.dto.CorrespondenciaRecibidaDto;
import consulting.hmd.application.map.CorrespondenciaRecibidaMap;
import consulting.hmd.controller.common.ControllerBase;
import consulting.hmd.exceptions.ServiceException;

import consulting.hmd.model.CorrespondenciaRecibida;
import consulting.hmd.services.ICorrespondenciaRecibidaService;

@RestController
@RequestMapping("/api/CorrespondenciaRecibida/v1")
public class CorrespondenciaRecibidaController extends ControllerBase<CorrespondenciaRecibida> {

	@Autowired
	ICorrespondenciaRecibidaService service;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CorrespondenciaRecibida>> listar() {
		listado = new ArrayList<>();
		try {
			listado = service.Obtener();
			return listado.size() > 0 ? new ResponseEntity<List<CorrespondenciaRecibida>>(listado, HttpStatus.OK)
					: new ResponseEntity<List<CorrespondenciaRecibida>>(new ArrayList<>(), HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<List<CorrespondenciaRecibida>>(new ArrayList<>(),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CorrespondenciaRecibida> listarPorId(@PathVariable("id") Integer id) {
		entidad = new CorrespondenciaRecibida();
		try {
			entidad = service.ObtenerPorId(id);

			return entidad == null
					? new ResponseEntity<CorrespondenciaRecibida>(new CorrespondenciaRecibida(), HttpStatus.NO_CONTENT)
					: new ResponseEntity<CorrespondenciaRecibida>(entidad, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<CorrespondenciaRecibida>(new CorrespondenciaRecibida(),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

//	@GetMapping(value = "/listar/nombre/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<List<CorrespondenciaRecibida>> listarPorNombre(@PathVariable("nombre") String nombre) {
//		listado = new ArrayList<>();
//		try {
//			listado = service.ObtenerPorNombre(nombre);
//			return listado.size() > 0 ? new ResponseEntity<List<CorrespondenciaRecibida>>(listado, HttpStatus.OK)
//					: new ResponseEntity<List<CorrespondenciaRecibida>>(new ArrayList<>(), HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity<List<CorrespondenciaRecibida>>(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrar(@RequestBody CorrespondenciaRecibidaDto dto) {
		codigo = 0;

		try {
			service.setUsuario(UsuarioLogeado());
			codigo = service.Crear(CorrespondenciaRecibidaMap.ToModel(dto));

			switch (codigo) {
			case 201:
				return new ResponseEntity<Integer>(codigo, HttpStatus.CREATED);
			case 500:
				return new ResponseEntity<Integer>(codigo, HttpStatus.INTERNAL_SERVER_ERROR);

			default:
				return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> actualizar(@RequestBody CorrespondenciaRecibidaDto dto) {

		codigo = 0;
		try {
			service.setUsuario(UsuarioLogeado());
			codigo = service.Modificar(CorrespondenciaRecibidaMap.ToModel(dto));

			switch (codigo) {
			case 200:
				return new ResponseEntity<Integer>(codigo, HttpStatus.CREATED);
			case 204:
				return new ResponseEntity<Integer>(codigo, HttpStatus.NO_CONTENT);
			case 500:
				return new ResponseEntity<Integer>(codigo, HttpStatus.INTERNAL_SERVER_ERROR);

			default:
				return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable Integer id) {
		codigo = 0;
		try {
			service.setUsuario(UsuarioLogeado());
			codigo = service.Eliminar(id);

			switch (codigo) {
			case 200:
				return new ResponseEntity<Integer>(codigo, HttpStatus.CREATED);
			case 500:
				return new ResponseEntity<Integer>(codigo, HttpStatus.INTERNAL_SERVER_ERROR);

			default:
				return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
