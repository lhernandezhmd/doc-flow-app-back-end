package consulting.hmd.model.seg;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.hmd.model.common.EntityBase;


@Entity
@Table(name = "seg_menu_principal", schema = "public")
public class MenuPrincipal extends EntityBase<Integer> {

	private static final long serialVersionUID = 1L;

	public MenuPrincipal() {
	}

	public MenuPrincipal(int id) {
		setId(id);
	}

	@Column(name = "titulo", nullable = false, length = 100)
	private String titulo;
	@Column(name = "icono", nullable = true, length = 20)
	private String icono;
	@Column(name = "url", nullable = true, length = 200)
	private String url;
	
	@ManyToOne
	@JoinColumn(name = "id_menu_padre", nullable = true)
	@JsonIgnore
	private MenuPrincipal menuPadre;
	
	@OneToMany(mappedBy = "menuPadre", fetch = FetchType.LAZY,  cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE })
	private List<MenuPrincipal>hijos;

	private boolean estado;


	@OneToMany(mappedBy = "menuPrincipal", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE })
	@JsonIgnore
	private List<AsignacionRolesMenuPrincipal> asignacionesRolesMenu;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public List<AsignacionRolesMenuPrincipal> getAsignacionesRolesMenu() {
		return asignacionesRolesMenu;
	}

	public void setAsignacionesRolesMenu(List<AsignacionRolesMenuPrincipal> asignacionesRolesMenu) {
		this.asignacionesRolesMenu = asignacionesRolesMenu;
	}

	public MenuPrincipal getMenuPadre() {
		return menuPadre;
	}

	public void setMenuPadre(MenuPrincipal menuPadre) {
		this.menuPadre = menuPadre;
	}

	public List<MenuPrincipal> getHijos() {
		return hijos;
	}

	public void setHijos(List<MenuPrincipal> hijos) {
		this.hijos = hijos;
	}
	
}
