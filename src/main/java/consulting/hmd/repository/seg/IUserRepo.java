package consulting.hmd.repository.seg;

import org.springframework.data.jpa.repository.JpaRepository;

import consulting.hmd.model.seg.Usuario;


public interface IUserRepo extends JpaRepository<Usuario, Integer> {
	
	Usuario findOneByUsername(String username);
}